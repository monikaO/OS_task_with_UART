#ifndef _SW1_H_
#define _SW1_H_

/*** macros that are used led 1 task ***/
#define SW1TASK_STATE_FLAG_PUSHED 1
#define SW1TASK_STATE_FLAG_RELEASED 0

/* Description: This function is used to implement switch 1 task */
void void_SW1_Task(void);

/* Description: This function is used to return switch state flag */
u8 u8_get_SW1_STATE_FLAG(void);

#endif
