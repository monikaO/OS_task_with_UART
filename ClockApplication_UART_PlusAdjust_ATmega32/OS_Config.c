#include "STD_TYPES.h"
#include "OS_Config.h"
#include "DIO_int.h"
#include "keypad_task.h"
#include "sevenSegmentsTask.h"
#include "led1Task.h"
#include "led2Task.h"
#include "sw1Task.h"
#include "sw2Task.h"
#include "SWITCH.h"
#include "switchModeTask.h"
#include "timeAdjustTask.h"
#include "timeCountTask.h"
#include "HandlingTasksUsingUART.h"
#include "BIT_MATH.h"
#include "DIO_register.h"

const OS_Config_T OS_Config[]=
{
		{taskTimeCount,250},
		{void_SW1_Task,20},
		{void_SW2_Task,20},
		{switchHandler,1},
		{void_keypad_task,20},
		{SW_Clock_Mode_Task,20},
		{taskTimeAdjust,20},
		{taskSegments,20},
		{void_LED1_Task,20},
		{void_LED2_Task,20},
		{TaskRecieve,10},
		{TaskTransmit,10}

};

const u8 Num_of_Tasks = sizeof(OS_Config)/sizeof(OS_Config_T);

