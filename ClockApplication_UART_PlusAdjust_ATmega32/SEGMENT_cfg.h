/* A guard for the header file to insure it will be included only once                  */
#ifndef SEGMENT_CFG_H
#define SEGMENT_CFG_H
/****************************************************************************************/

typedef struct
{
	u8 SEGMENT_segmentsConnections[8];
	u8 SEGMENT_commonConnection;
	u8 SEGMENT_type;
	
}SEGMENT_cfg_t;

extern const SEGMENT_cfg_t segmentsArray[];
extern const u8 sevSegDataAnode[];
extern const u8 sevSegDataCathode[];
#endif
