#include "STD_TYPES.h"
#include "BIT_MATH.h"

#include "DIO_register.h"

#include "DIO_config.h"
#include "DIO_priv.h"
#include "DIO_int.h"

/* Description: This function reads a specific pin */
u8 DIO_u8GetPinValue(u8 u8PinIndexCPY)
{

	/****************************************/


	/*check if the pin index in port A range*/
	u8 val;
	if((u8PinIndexCPY>=DIO_u8_PORTA_START)&&(u8PinIndexCPY<=DIO_u8_PORTA_END))
	{

		val=GET_BIT(PINA,u8PinIndexCPY);

		if( DIO_u8_HIGH==0&&DIO_u8_LOW==1)
		{
			val=!val;
		}
		else
		{

		}
	}
	else if((u8PinIndexCPY>=DIO_u8_PORTB_START)&&(u8PinIndexCPY<=DIO_u8_PORTB_END))
	{
		 u8PinIndexCPY-=(DIO_U8_PORTA_SIZE);
		 val=GET_BIT(PINB,u8PinIndexCPY);

		if(  DIO_u8_HIGH==0&&DIO_u8_LOW==1)
		{
			val=!val;
		}
		else
		{

		}
	}

	else if((u8PinIndexCPY>=DIO_u8_PORTC_START)&&(u8PinIndexCPY<=DIO_u8_PORTC_END))
	{
		 u8PinIndexCPY-=(DIO_U8_PORTA_SIZE+DIO_U8_PORTB_SIZE);

		 val=GET_BIT(PINC,u8PinIndexCPY);
		if(  DIO_u8_HIGH==0&&DIO_u8_LOW==1)
		{
			val=!val;
		}
		else
		{

		}
	}
		else if((u8PinIndexCPY>=DIO_u8_PORTD_START)&&(u8PinIndexCPY<=DIO_u8_PORTD_END))
	{
		 u8PinIndexCPY-=(DIO_U8_PORTA_SIZE+DIO_U8_PORTB_SIZE+DIO_U8_PORTC_SIZE);

		 val=GET_BIT(PIND,u8PinIndexCPY);
		if( DIO_u8_HIGH==0&&DIO_u8_LOW==1)
		{
			val=!val;
		}
		else
		{

		}
	}
	else
	{

	}

	/*****************************************/


return val;
}

/* Description: This function sets a specific pin with a specific value */
void DIO_voidSetPinValue(u8 u8PinIndexCPY,u8  u8PinValueCPY)
{
	/*check if the pin index in port A range*/
	if((u8PinIndexCPY>=DIO_u8_PORTA_START)&&(u8PinIndexCPY<=DIO_u8_PORTA_END))
	{
		if( u8PinValueCPY==DIO_u8_HIGH)
		{
			SET_BIT(PORTA,  u8PinIndexCPY);
		}
		else
		{
			CLR_BIT(PORTA,  u8PinIndexCPY);		
		}
	}
	else if((u8PinIndexCPY>=DIO_u8_PORTB_START)&&(u8PinIndexCPY<=DIO_u8_PORTB_END))
	{
		 u8PinIndexCPY-=(DIO_U8_PORTA_SIZE);
		 
		if( u8PinValueCPY==DIO_u8_HIGH)
		{
			SET_BIT(PORTB, u8PinIndexCPY );
		}
		else
		{
			CLR_BIT(PORTB, u8PinIndexCPY);		
		}
	}
	
	else if((u8PinIndexCPY>=DIO_u8_PORTC_START)&&(u8PinIndexCPY<=DIO_u8_PORTC_END))
	{
		 u8PinIndexCPY-=(DIO_U8_PORTA_SIZE+DIO_U8_PORTB_SIZE);
		 
		if( u8PinValueCPY==DIO_u8_HIGH)
		{
			SET_BIT(PORTC, u8PinIndexCPY );
		}
		else
		{
			CLR_BIT(PORTC, u8PinIndexCPY);		
		}
	}
		else if((u8PinIndexCPY>=DIO_u8_PORTD_START)&&(u8PinIndexCPY<=DIO_u8_PORTD_END))
	{
		 u8PinIndexCPY-=(DIO_U8_PORTA_SIZE+DIO_U8_PORTB_SIZE+DIO_U8_PORTC_SIZE);
		 
		if( u8PinValueCPY==DIO_u8_HIGH)
		{
			SET_BIT(PORTD, u8PinIndexCPY );
		}
		else
		{
			CLR_BIT(PORTD, u8PinIndexCPY);		
		}
	}
	else
	{
		
	}
}

/* Description: This function initializes pins directions for each port */
void DIO_voidInitialize(void)
{
	DDRA=DIO_U8_PORTA_DIRECTION;
	DDRB=DIO_U8_PORTB_DIRECTION;
	DDRC=DIO_U8_PORTC_DIRECTION;
	DDRD=DIO_U8_PORTD_DIRECTION;
}
