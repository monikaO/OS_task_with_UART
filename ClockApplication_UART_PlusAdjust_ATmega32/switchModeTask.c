#include "STD_TYPES.h"
#include "keypad_task.h"
#include "switchModeTask.h"

#include "HandlingTasksUsingUART.h"
volatile static u8 u8SwModeFlag;

/*this task check the keypad to switch the clock display mode on 7 segment*/
void SW_Clock_Mode_Task(void)
{
  /* get pressed key */
	u8 u8pressedKEY=i38_getKeypadPressedKey();
  
  /* check if chosen mode is seconds */
	if(u8pressedKEY==Change_MODE_TO_SEC_OPTION)
	{
    /* if seconds mode, update the static variable */
		u8SwModeFlag=u8_SEC_MODE;
	}
  
  /* check if chosen mode is minutes */
	else if(u8pressedKEY==Change_MODE_TO_MIN_OPTION)
	{
    /* if minutes mode, update the static variable */
		u8SwModeFlag=u8_MIN_MODE;
	}
  
  /* check if chosen mode is hours */
	else if(u8pressedKEY==Change_MODE_TO_HR_OPTION)
	{
    /* if hours mode, update the static variable */
		u8SwModeFlag=u8_HR_MODE;
	}
	else
	{
		/*do nothing*/
	}
}

/*this function returns the current clock display mode*/
/*0 for sec*/
/*1 for min*/
/*2 for hr */
/*you can use the #defines in the switchMoseTask.h instead*/
u8 u8_get_Clock_mode()
{
	return u8SwModeFlag;
}
