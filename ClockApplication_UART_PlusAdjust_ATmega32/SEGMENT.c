#include "STD_TYPES.h"
#include "BIT_MATH.h"

#include "DIO_int.h"

#include "SEGMENT.h"

/* This function is used to output a specific number on the seven segment				*/
void SEGMENT_voidDisplayNumber(u8 u8NumberCpy, u8 u8SegmentIndex)
{
	u8 u8loopIndexLoc;
	u8 bitLoc;

	for(u8loopIndexLoc = 0;u8loopIndexLoc<8;u8loopIndexLoc++)
	{

		if(segmentsArray[u8SegmentIndex].SEGMENT_type == 1)//common anode
		{
			bitLoc = GET_BIT(sevSegDataAnode[u8NumberCpy],u8loopIndexLoc);
		}
		else //common cathode
		{
			bitLoc = GET_BIT(sevSegDataCathode[u8NumberCpy],u8loopIndexLoc);
		}

		if(bitLoc == 1)
		{
			DIO_voidSetPinValue(segmentsArray[u8SegmentIndex].SEGMENT_segmentsConnections[u8loopIndexLoc],DIO_u8_HIGH);
		}
		else
		{
			DIO_voidSetPinValue(segmentsArray[u8SegmentIndex].SEGMENT_segmentsConnections[u8loopIndexLoc],DIO_u8_LOW);
		}
	}
}

/* This function is used to enable the seven segment									*/
void SEGMENT_voidEnable(u8 u8SegmentIndex)
{
	if(segmentsArray[u8SegmentIndex].SEGMENT_type == 1) //common anode
	{
		DIO_voidSetPinValue(segmentsArray[u8SegmentIndex].SEGMENT_commonConnection,DIO_u8_HIGH);
	}
	else // common cathode
	{
		DIO_voidSetPinValue(segmentsArray[u8SegmentIndex].SEGMENT_commonConnection,DIO_u8_LOW);
	}
}

/* This function is used to disable the seven segment									*/
void SEGMENT_voidDisable(u8 u8SegmentIndex)
{
	if(segmentsArray[u8SegmentIndex].SEGMENT_type == 1) //common anode
	{
		DIO_voidSetPinValue(segmentsArray[u8SegmentIndex].SEGMENT_commonConnection,DIO_u8_LOW);
	}
	else // common cathode
	{
		DIO_voidSetPinValue(segmentsArray[u8SegmentIndex].SEGMENT_commonConnection,DIO_u8_HIGH);
	}
}
