#include "Timer.h"

#include "STD_TYPES.h"
#include "BIT_MATH.h"

#include "Timer.h"

#include "DIO_register.h"

#define TCNT0 *((volatile u8*)0x52)
#define TCCR0 *((volatile u8*)0x53)
#define TIMSK *((volatile u8*)0x59)
#define TIFR  *((volatile u8*)0x58)
#define OCR0  *((volatile u8*)0x5C)

/******************************************/
/* Description: This function is used to  */
/* 				initialize the timer 	  */
/* 				peripheral				  */
/******************************************/
static void (*Timer_CallBack) (void);

/******************************************/
/* Description: This function is used to  */
/* 				initialize the timer 	  */
/* 				peripheral				  */
/******************************************/
extern void Timer_init(void)
{
	/* Clear Flag */
	SET_BIT(TIFR,1);

	/*Disable Interrupt*/
	CLR_BIT(TIMSK,1);

	/*Counter Initial value */
	TCNT0= TimerInitialValue;

	/* Compare Value */
	OCR0 = CompareValue;

	/*Set Pre-Scaler to 64 and Mode To CTC*/
	TCCR0  = 0x0C;

}

/******************************************/
/* Description: This function is used to  */
/* 				Enable timer interrupt	  */
/******************************************/
extern void Timer_EnableInterrupt(void)
{
	SET_BIT(TIMSK,1);
}

/******************************************/
/* Description: This function is used to  */
/* 				Enable timer interrupt	  */
/******************************************/
extern void Timer_DisableInterrupt(void)
{
	CLR_BIT(TIMSK,1);
}

/******************************************/
/* Description: This function is used to  */
/* 				set timer call back 	  */
/* 				function 	  			  */
/******************************************/
extern void Timer_SetCallBack(void (*ptrCpy) (void))
{
	Timer_CallBack = ptrCpy;
}

void __vector_10 (void) __attribute__((signal,used));
void __vector_10 (void)
{
	Timer_CallBack();
}
