#ifndef SWITCH_H_
#define SWITCH_H_

#include "SWITCH_cfg.h"

/******************************************/
/* Description: This hash define is used  */
/* 				to set  the values of 	  */
/* 				pushed, released and 	  */
/* 				error values			  */
/******************************************/
#define SWITCH_u8Released  	1
#define SWITCH_u8Pushed		50
#define SWITCH_u8Error 		100

/******************************************/
/* Description: This function is used to  */
/* 				return the state of		  */
/*              switch pushed or released */
/******************************************/
u8 GetSwitchState(u8 u8SwitchIndexCpy);

/*Description:this function should be called by OS
 * 				it takes void and returns void
 * 				the purpose is to handle sw bouncing
 * 				the period is 5 ms
 */
void switchHandler(void);

#endif /* SWITCH_H_ */
