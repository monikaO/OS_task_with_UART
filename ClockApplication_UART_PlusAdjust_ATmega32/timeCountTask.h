#ifndef TIMESCOUNT_H
#define TIMESCOUNT_H

/* Description: This function implements the counting time task */
extern void taskTimeCount(void);

/* Description: This function returns the current seconds count */
extern u8 getSeconds(void);

/* Description: This function returns the current minutes count */
extern u8 getMinutes(void);

/* Description: This function returns the current hours count */
extern u8 getHours(void);

/* Description: This function sets the current seconds count */
extern void setSeconds(u8 secondsCpy);

/* Description: This function sets the current minutes count */
extern void setMinutes(u8 minutesCpy);

/* Description: This function sets the current hours count */
extern void setHours(u8 hoursCpy);

#endif
