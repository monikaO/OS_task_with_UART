/*******************************************************/
/*author ahmed raafat*/
/*date 28 feb 2018*/
/*version v01*/
/*******************************************************/
#ifndef _SW2_H_
#define _SW2_H_

/*** macros that are used led 2 task ***/
#define SW2TASK_STATE_FLAG_PUSHED 1
#define SW2TASK_STATE_FLAG_RELEASED 0

/* Description: This function is used to implement switch 2 task */
void void_SW2_Task(void);

/* Description: This function is used to return switch state flag */
u8 u8_get_SW2_STATE_FLAG(void);

#endif
