#ifndef  BIT_MATH_H
#define  BIT_MATH_H

#define SET_BIT(x,bitNum) (x)|=(1<<(bitNum))
#define CLR_BIT(x,bitNum) (x)&=~((1<<bitNum))
#define TOG_BIT(x,bitNum) (x)^=(1<<(bitNum))
#define GET_BIT(x,bitNum)   ( (x)&(1<<(bitNum)) )  >>(bitNum)     

#define BIT_CONC(B0,B1,B2,B3,B4,B5,B6,B7) CONC_HELP(B0,B1,B2,B3,B4,B5,B6,B7)
#define CONC_HELP(B0,B1,B2,B3,B4,B5,B6,B7) 0b##B7##B6##B5##B4##B3##B2##B1##B0

#endif
