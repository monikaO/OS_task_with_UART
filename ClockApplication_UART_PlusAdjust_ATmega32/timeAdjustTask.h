#ifndef TIMEADJUST_H
#define TIMEADJUST_H

/* Description: This function implements the adjusting time task */
extern void taskTimeAdjust(void);

#endif
