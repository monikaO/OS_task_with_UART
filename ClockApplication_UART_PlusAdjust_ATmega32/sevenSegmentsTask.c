#include "STD_TYPES.h"

#include "SEGMENT.h"

#include "HandlingTasksUsingUART.h"
#include "keypad_task.h"
#include "timeAdjustTask.h"
#include "timeCountTask.h"
#include "switchModeTask.h"
#include "sevenSegmentsTask.h"

/* Description: This function implements the seven segments task */ 
extern void taskSegments(void)
{
	u8 seconds, minutes, hours;

	u8 flag=u8_get_Clock_mode();

	/* depending on which is chosen now display the chosen (getters) */
	if(flag ==u8_SEC_MODE)
	{
		seconds = getSeconds();
		SEGMENT_voidDisplayNumber(seconds/10, 1);
		SEGMENT_voidDisplayNumber(seconds%10, 0);
	}
	else if(flag ==u8_MIN_MODE)
	{
		minutes = getMinutes();
		SEGMENT_voidDisplayNumber(minutes/10, 1);
		SEGMENT_voidDisplayNumber(minutes%10, 0);
	}
	else if(flag ==u8_HR_MODE)
	{
		hours = getHours();
		SEGMENT_voidDisplayNumber(hours/10, 1);
		SEGMENT_voidDisplayNumber(hours%10, 0);
	}

	else
	{
		/* Do nothing */
	}

}
