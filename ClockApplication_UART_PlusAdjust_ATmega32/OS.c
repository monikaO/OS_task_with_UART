#include "STD_TYPES.h"
#include "OS.h"
#include "Timer.h"

#include "sw1Task.h"
#include "sw2Task.h"
#include "LEDS.h"

static volatile u8 CTC_Flag=0;
static volatile u32 CTC_counter=0;

static void OS_ISR(void);

/******************************************/
/* Description: This function is used to  */
/* 				initialize to calculate   */
/* 				number of tasks  		  */
/******************************************/
extern void OS_init(void)
{
	 Timer_SetCallBack(OS_ISR);
}

/******************************************/
/* Description: This function is used to  */
/* 				Start OS in main		  */
/******************************************/
extern void OS_Start(void)
{
	static u8 i;

	while(1)
	{

		if(1==CTC_Flag)
		{
			CTC_Flag=0;
			for(i=0;i<Num_of_Tasks;i++)
			{
				if(CTC_counter%OS_Config[i].Periodicity==0)
				{
					OS_Config[i].pftask();
				}
			}
		}
	}
}

/******************************************/
/* Description: This function is used to  */
/* 				OS ISR 				  	  */
/******************************************/
static void OS_ISR(void)
{

	CTC_counter++;
	CTC_Flag = 1;
}
