#include "STD_TYPES.h"
#include "BIT_MATH.h"
#include "KEYPAD.h"
#include "keypad_task.h"

volatile static u8 u8_pressed_key;
volatile static u8 u8_pressed_key_temp;
volatile static u8 u8pressCurrent;
volatile static u8 u8pressPrev;
volatile static u16 u16PressCounter;
volatile static u16 u16ReleaseCounter;

/*description: this function do the keypad task update the flags and should be called by os every 100ms*/
void void_keypad_task(void)
{
	u8 i;
	u16 u16_keypad_val;
	u8pressPrev=u8pressCurrent;

	u16_keypad_val=KEYPAD_u16GetStatus();

	/*******************************/

	for(i=0;i<16;i++)
	{
		if(GET_BIT(u16_keypad_val,i)==1)
		{
			u8_pressed_key_temp=i;
			break;
		}
		else
		{
			//this means keypad isn't pressed
			u8_pressed_key_temp=16;
		}

	}


	u8pressCurrent=u8_pressed_key_temp;

	/*if new release reset the release counter*/
	if( (u8pressCurrent==16)&& (u8pressCurrent!=u8pressPrev))
	{
		u16ReleaseCounter=0;
	}
	/*if still in the release increment the release counter*/
	else if( (u8pressCurrent==16)&& (u8pressCurrent==u8pressPrev))
	{
		u16ReleaseCounter++;
	}
	else
	{
		//do nothing
	}


	/*if new press reset the press counter*/
	if( (u8pressCurrent==16)&& (u8pressCurrent!=u8pressPrev))
	{
		u16PressCounter=0;
	}
	/*if still in the press increment the press counter*/
	else if( (u8pressCurrent!=16)&& (u8pressCurrent==u8pressPrev))
	{
		u16PressCounter++;
	}
	else
	{
		//do nothing
	}

}

/*description: this function returns the first pressed key on the keypad*/
u8 u8_get_first_pressed_key(void)
{
	switch(u8_pressed_key_temp)
	{

	case 0:
		u8_pressed_key=Change_MODE_TO_SEC_OPTION;

		break;

	case 1:
		u8_pressed_key=Change_MODE_TO_MIN_OPTION;

		break;

	case 2:
		u8_pressed_key=0;
		break;

	case 3:
		u8_pressed_key=Change_MODE_TO_HR_OPTION;

		break;

	case 4:
		u8_pressed_key= ADJUST_SEC_OPTION;
		break;

	case 5:
		u8_pressed_key=9;
		break;
    
	case 6:
		u8_pressed_key=8;
		break;
    
	case 7:
		u8_pressed_key=7;
		break;

	case 8:
		u8_pressed_key= ADJUST_MIN_OPTION ;
		break;

	case 9:
		u8_pressed_key=6;
		break;
    
	case 10:
		u8_pressed_key=5;
		break;
    
	case 11:
		u8_pressed_key=4;
		break;

	case 12:
		u8_pressed_key= ADJUST_HR_OPTION ;
		break;

	case 13:
		u8_pressed_key=3;
		break;

	case 14 :
		u8_pressed_key=2;
		break;

	case 15:
		u8_pressed_key=1;
		break;

	case 16:
		u8_pressed_key=KEYPAD_NOT_PRESSED;
		break;

	default:
		u8_pressed_key=KEYPAD_ERROR;
		break;

	}
	return u8_pressed_key;
}

/*this function returns 1 if it is the first time to press the switch or 0 otherwise*/
u8 u8_first_time_pressed(void)
{
	if(u8pressCurrent!=u8pressPrev)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

/*this function returns 1 if it is the first timer to release the switch or 0 otherwise*/
u8 u8_first_time_released(void)
{
	if(u8pressCurrent!=u8pressPrev)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}


/*this function returns the number of ticks from the first press to the function calling time*/
u16 u16_get_press_time(void)
{
	return u16PressCounter;
}

/*this function returns the number of ticks from the first release to the function calling time*/
u16 u16_get_release_time(void)
{
	return u16ReleaseCounter;
}
