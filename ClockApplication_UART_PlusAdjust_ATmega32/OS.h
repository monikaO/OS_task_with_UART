#ifndef OS_H_
#define OS_H_

#include "OS_Config.h"

/******************************************/
/* Description: This function is used to  */
/* 				initialize but its empty  */
/******************************************/
extern void OS_init(void);

/******************************************/
/* Description: This function is used to  */
/* 				Start OS in main		  */
/******************************************/
extern void OS_Start(void);

#endif /* OS_H_ */
