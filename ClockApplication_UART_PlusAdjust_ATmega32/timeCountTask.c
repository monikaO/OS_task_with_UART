#include "STD_TYPES.h"
#include "timeAdjustTask.h"
#include "timeCountTask.h"

volatile static u8 seconds = 0;
volatile u8 minutes = 0;
volatile u8 hours = 0;

/* Description: This function implements the counting time task */
extern void taskTimeCount(void)
{
  u8 secondsLoc, minutesLoc, hoursLoc;
  
  /* get seconds, minutes and hours count*/
  secondsLoc = getSeconds();
  minutesLoc = getMinutes();
  hoursLoc = getHours();

  /* update the seconds count */
  secondsLoc++;
    
  /* check if seconds reached 60 */  
  if(secondsLoc>=60)
  {
    secondsLoc = 0;
    minutesLoc++;
  }

  /* check if minutes reached 60 */
  if(minutesLoc>=60)
  {
    minutesLoc = 0;
    hoursLoc++;
  }
    
  /* check if hours reached 24 */
  if(hoursLoc>=24)
  {
    hoursLoc = 0;
  }        

  /* set the updated seconds, minutes and hours count */ 
  setSeconds(secondsLoc);
  setMinutes(minutesLoc);
  setHours(hoursLoc);
}

/* Description: This function returns the current seconds count */
extern u8 getSeconds(void)
{
  return seconds;
}

/* Description: This function returns the current minutes count */
extern u8 getMinutes(void)
{
  return minutes;
}

/* Description: This function returns the current hours count */
extern u8 getHours(void)
{
  return hours;
}

/* Description: This function sets the current seconds count */
extern void setSeconds(u8 secondsCpy)
{
  seconds = secondsCpy;
}

/* Description: This function sets the current minutes count */
extern void setMinutes(u8 minutesCpy)
{
  minutes = minutesCpy;
}

/* Description: This function sets the current hours count */
extern void setHours(u8 hoursCpy)
{
  hours = hoursCpy;
}
