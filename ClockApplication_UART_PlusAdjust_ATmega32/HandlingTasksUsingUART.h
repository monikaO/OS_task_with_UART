#ifndef HANDLINGTASKSUSINGUART_H_
#define HANDLINGTASKSUSINGUART_H_

/******* macros for the frames ********/
#define SwitchOnePressed			0x01
#define SwitchOneReleased			0x00

#define SwitchTwoPressed			0x10
#define SwitchTwoReleased			0x11

#define SwitchToSecondPressed		0x40
#define SwitchToMinutesPressed		0x41
#define SwitchToHoursPressed		0x42

#define AdjustSecondPressed			0x30
#define AdjustMinutesPressed		0x31
#define AdjustHoursPressed			0x32

#define KeypadValue_0				0x20
#define KeypadValue_1				0x21
#define KeypadValue_2				0x22
#define KeypadValue_3				0x23
#define KeypadValue_4				0x24
#define KeypadValue_5				0x25
#define KeypadValue_6				0x26
#define KeypadValue_7				0x27
#define KeypadValue_8				0x28
#define KeypadValue_9				0x29

#define KEYPADValue_NOT_PRESSED			0x2A
/********************************************/

/* this task implements the transmitting to UART */
extern void TaskTransmit(void);

/* this task implements the reception from UART */
extern void TaskRecieve(void);

/* Description: This function should return switch 1 state */
extern u8 i38_getSW1State(void);

/* Description: This function should return switch 2 state */
extern u8 i38_getSW2State(void);

/* Description: This function should return the current pressed keypad key */
extern u8 i38_getKeypadPressedKey(void);

#endif /* HANDLINGTASKSUSINGUART_H_ */
