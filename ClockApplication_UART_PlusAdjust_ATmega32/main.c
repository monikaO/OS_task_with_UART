#include "STD_TYPES.h"
#include "BIT_MATH.h"
#include "DIO_register.h"
#include "DIO_int.h"
#include "LEDS.h"
#include "OS.h"
#include "Timer.h"
#include "SEGMENT.h"
#include "GIE_int.h"
#include "sw1Task.h"
#include "sw2Task.h"
#include "KEYPAD.h"
#include "uart.h"

void main(void)
{
	u8 i;
	DIO_voidInitialize();
	UART_init();
	LEDS_voidInitialize();

	SEGMENT_voidEnable(0);
	SEGMENT_voidEnable(1);

   // enable internal pull up for keypad inputs
	DIO_voidSetPinValue(DIO_u8_PIN_14,DIO_u8_HIGH);
	DIO_voidSetPinValue(DIO_u8_PIN_15,DIO_u8_HIGH);
	DIO_voidSetPinValue(DIO_u8_PIN_26,DIO_u8_HIGH);
	DIO_voidSetPinValue(DIO_u8_PIN_27,DIO_u8_HIGH);


	 //enable internal pull up for switches
	DIO_voidSetPinValue(DIO_u8_PIN_12,DIO_u8_HIGH);
	DIO_voidSetPinValue(DIO_u8_PIN_13,DIO_u8_HIGH);

	Timer_init();
	Timer_EnableInterrupt();
	GIE_voidSetGIE();
	OS_init();
	OS_Start();

	while(1)
	{


	}
}





