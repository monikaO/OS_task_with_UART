#include "STD_TYPES.h"
#include "BIT_MATH.h"

#include "DIO_int.h"
#include "LEDS.h"

/*Initializing LEDS*/
void LEDS_voidInitialize(void)
{
  for(u8 i = 0;i<led_max;i++)
  {

    /* Checking state of led 0*/
    if( leds[i].led_init_state == LEDS_u8_STATE_HIGH)
    {
      LEDS_voidSetLedOn(i);
    }
    else
    {
      LEDS_voidSetLedOff(i);
    }
  }
}


/* Turning chosen LED on*/
void LEDS_voidSetLedOn(u8 u8LedPinIndex)
{
	/*Checking if u8LedPinIndex is within led index*/
	if(u8LedPinIndex < led_max)
	{
		if( leds[u8LedPinIndex].led_mode  == LEDS_u8_MODE_NORMAL)
		{
			DIO_voidSetPinValue(leds[u8LedPinIndex].led_pin,DIO_u8_HIGH);
		}
		else 
		{
			DIO_voidSetPinValue(leds[u8LedPinIndex].led_pin,DIO_u8_LOW);
		}
	}	
}


/* Turning chosen LED off*/
void LEDS_voidSetLedOff(u8 u8LedPinIndex)
{
	/*Checking if u8LedPinIndex is within led index*/
	if(u8LedPinIndex < led_max)
	{
		if( leds[u8LedPinIndex].led_mode  == LEDS_u8_MODE_NORMAL)
		{
			DIO_voidSetPinValue(leds[u8LedPinIndex].led_pin,DIO_u8_LOW);
		}
		else 
		{
			DIO_voidSetPinValue(leds[u8LedPinIndex].led_pin,DIO_u8_HIGH);
		}
	}
}
