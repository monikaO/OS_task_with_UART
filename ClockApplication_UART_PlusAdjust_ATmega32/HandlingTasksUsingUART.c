#include "STD_TYPES.h"
#include "UART.h"
#include "HandlingTasksUsingUART.h"
#include "SWITCH.h"
#include "keypad_task.h"
#include "timeAdjustTask.h"
#include "sw1Task.h"
#include "sw2Task.h"

/****** declaration of variables used in receive task ****/
volatile static u8 sw1State = SWITCH_u8Released;
volatile static u8 sw2State = SWITCH_u8Released;

volatile static u8 key_pressed = KEYPADValue_NOT_PRESSED;

volatile static u8 u8pressCurrent;

volatile static u8 u8pressPrev;

u8 data[1];
u16 len;

/******* declaration of variables used in transmit task ****/
volatile static u8 sw1Current = SW1TASK_STATE_FLAG_RELEASED;
volatile static u8 sw2Current = SW2TASK_STATE_FLAG_RELEASED;

volatile static u8 sw1Previous = SW1TASK_STATE_FLAG_RELEASED;
volatile static u8 sw2Previous = SW2TASK_STATE_FLAG_RELEASED;

volatile static u8 TransmitData[1]={0};

volatile static u8 KeyPadCurrent = KEYPAD_NOT_PRESSED;
volatile static u8 KeyPadPrevious = KEYPAD_NOT_PRESSED;

/* this task implements the reception from UART */
extern void TaskRecieve(void)
{
	/* call UART receive */
	UART_receive(data,&len);

	/* check if there is data received,  len is greater than zero */
	if(len!=0)
	{
		/* equate the previous pressed key with the current one */
		u8pressPrev = u8pressCurrent;

		/* switch on first byte received */
		switch(data[0])
		{

		/* case first key is pressed */
		case SwitchOnePressed:
			sw1State = SWITCH_u8Pushed;
			break;

		/* case first key is released */
		case SwitchOneReleased:
			sw1State = SWITCH_u8Released;
			break;

		/* case second switch is pressed */
		case SwitchTwoPressed:
			sw2State = SWITCH_u8Pushed;
			break;

		/* case second switch is released */
		case SwitchTwoReleased:
			sw2State = SWITCH_u8Released;
			break;

		/* case keypad switch to seconds is pressed */
		case SwitchToSecondPressed:
			key_pressed = Change_MODE_TO_SEC_OPTION;
			break;

		/* case keypad switch to minutes is pressed */
		case SwitchToMinutesPressed:
			key_pressed = Change_MODE_TO_MIN_OPTION;
			break;

		/* case keypad switch to hours is pressed */
		case SwitchToHoursPressed:
			key_pressed = Change_MODE_TO_HR_OPTION;
			break;

		/* case keypad adjust to seconds is pressed */
		case AdjustSecondPressed:
			key_pressed = ADJUST_SEC_OPTION;
			break;

		/* case keypad adjust to minutes is pressed */
		case AdjustMinutesPressed:
			key_pressed = ADJUST_MIN_OPTION;
			break;

		/* case keypad adjust to hours is pressed */
		case AdjustHoursPressed:
			key_pressed = ADJUST_HR_OPTION;
			break;

		/* case keypad number 0 is pressed */
		case KeypadValue_0:
			key_pressed = 0;
			break;

		/* case keypad number 1 is pressed */
		case KeypadValue_1:
			key_pressed = 1;
			break;

		/* case keypad number 2 is pressed */
		case KeypadValue_2:
			key_pressed = 2;
			break;

		/* case keypad number 3 is pressed */
		case KeypadValue_3:
			key_pressed = 3;
			break;

		/* case keypad number 4 is pressed */
		case KeypadValue_4:
			key_pressed = 4;
			break;

		/* case keypad number 5 is pressed */
		case KeypadValue_5:
			key_pressed = 5;
			break;

		/* case keypad number 6 is pressed */
		case KeypadValue_6:
			key_pressed = 6;
			break;

		/* case keypad number 7 is pressed */
		case KeypadValue_7:
			key_pressed = 7;
			break;

		/* case keypad number 8 is pressed */
		case KeypadValue_8:
			key_pressed = 8;
			break;

		/* case keypad number 9 is pressed */
		case KeypadValue_9:
			key_pressed = 9;
			break;

		/* case no keypad keys are pressed */
		case KEYPADValue_NOT_PRESSED:
			key_pressed = KEYPAD_NOT_PRESSED;
			break;
		default:
			break;

		}

		/* save the key pressed in current key */
		u8pressCurrent = key_pressed;
	}
}

/* this task implements the transmitting to UART */
extern void TaskTransmit(void)
{
	/* get sw1 state */
	sw1Current = u8_get_SW1_STATE_FLAG();

	/* get sw2 state */
	sw2Current = u8_get_SW2_STATE_FLAG();

	/* get keypad pressed key */
	KeyPadCurrent = u8_get_first_pressed_key();

	/* compare switch 1 state previous with the current state */
	if(sw1Current!=sw1Previous)
	{
		/* if states are not equal, check on the switch current state */
		if(sw1Current == SW1TASK_STATE_FLAG_PUSHED)
		{
			/* save the byte of switch 1 pressed to transmit it through UART */
			TransmitData[0]=SwitchOnePressed;
			UART_transmit(TransmitData,1);
		}
		else
		{
			/* save the byte of switch 1 released to transmit it through UART */
			TransmitData[0]=SwitchOneReleased;
			UART_transmit(TransmitData,1);
		}
	}

	/* compare switch 2 state previous with the current state */
	if(sw2Current!=sw2Previous)
	{
		/* if states are not equal, check on the switch current state */
		if(sw2Current == SW2TASK_STATE_FLAG_PUSHED)
		{
			/* save the byte of switch 2 pressed to transmit it through UART */
			TransmitData[0]=SwitchTwoPressed;
			UART_transmit(TransmitData,1);
		}
		else
		{
			/* save the byte of switch 2 released to transmit it through UART */
			TransmitData[0]=SwitchTwoReleased;
			UART_transmit(TransmitData,1);
		}
	}

	/* compare keypad previous pressed key with the current */
	if(KeyPadCurrent!=KeyPadPrevious)
	{
		/* switch on current pressed key */
		switch(KeyPadCurrent)
		{

		/* case switch to seconds is pressed */
		case Change_MODE_TO_SEC_OPTION:
			TransmitData[0]=SwitchToSecondPressed;
			break;

		/* case switch to minutes is pressed */
		case Change_MODE_TO_MIN_OPTION:
			TransmitData[0]=SwitchToMinutesPressed;
			break;

		/* case switch to hours is pressed */
		case Change_MODE_TO_HR_OPTION:
			TransmitData[0]=SwitchToHoursPressed;
			break;

		/* case adjust seconds is pressed */
		case ADJUST_SEC_OPTION:
			TransmitData[0]=AdjustSecondPressed;
			break;

		/* case adjust minutes is pressed */
		case ADJUST_MIN_OPTION:
			TransmitData[0]=AdjustMinutesPressed;
			break;

		/* case adjust hours is pressed */
		case ADJUST_HR_OPTION:
			TransmitData[0]=AdjustHoursPressed;
			break;

		/* case 0 is pressed */
		case 0:
			TransmitData[0]=KeypadValue_0;
			break;

		/* case 1 is pressed */
		case 1:
			TransmitData[0]=KeypadValue_1;
			break;

		/* case 2 is pressed */
		case 2:
			TransmitData[0]=KeypadValue_2;
			break;

		/* case 3 is pressed */
		case 3:
			TransmitData[0]=KeypadValue_3;
			break;

		/* case 4 is pressed */
		case 4:
			TransmitData[0]=KeypadValue_4;
			break;

		/* case 5 is pressed */
		case 5:
			TransmitData[0]=KeypadValue_5;
			break;

		/* case 6 is pressed */
		case 6:
			TransmitData[0]=KeypadValue_6;
			break;

		/* case 7 is pressed */
		case 7:
			TransmitData[0]=KeypadValue_7;
			break;

		/* case 8 is pressed */
		case 8:
			TransmitData[0]=KeypadValue_8;
			break;

		/* case 9 is pressed */
		case 9:
			TransmitData[0]=KeypadValue_9;
			break;

		/* case no key is pressed */
		case KEYPAD_NOT_PRESSED:
			TransmitData[0]=KEYPADValue_NOT_PRESSED;
			break;
		default:
			break;
		}

		/* transmit the data according to the change that occurred */
		UART_transmit(TransmitData,1);
	}

	/* save current states in previous states to compare the next iteration */
	KeyPadPrevious=KeyPadCurrent;
	sw1Previous = sw1Current;
	sw2Previous = sw2Current;

}

/* Description: This function should return switch 1 state */
extern u8 i38_getSW1State(void)
{
	return sw1State;
}

/* Description: This function should return switch 2 state */
extern u8 i38_getSW2State(void)
{
	return sw2State;
}

/* Description: This function should return the current pressed keypad key */
extern u8 i38_getKeypadPressedKey(void)
{
	return key_pressed;
}
