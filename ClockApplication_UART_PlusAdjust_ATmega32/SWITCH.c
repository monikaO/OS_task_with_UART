#include "STD_TYPES.h"
#include "DIO_int.h"
#include "SWITCH.h"

/*array hold the count of the previous sw samples if there are the same
 * if the state changes the counter wil be reseted
 * the purpose of this part is to handle the bouncing effect
 */
u8 count[SWITCH_u8_SW_NB];

/*define array that holds the current state of all the switches
 * the state should be update through the switch handler task*/
u8 swCurrentState[SWITCH_u8_SW_NB];

/*this function should return the value directly*/
u8 GetSwitchState(u8 u8SwitchIndexCpy)
{

	u8 u8Result;

	if(u8SwitchIndexCpy<SWITCH_u8_SW_NB)
	{

		u8Result =swCurrentState[ u8SwitchIndexCpy];

		if(u8Result ==  SWITCH_INSTANCES[u8SwitchIndexCpy].SWITCH_u8SwitchOpenState)
		{
			u8Result = SWITCH_u8Released;
		}
		else
		{
			u8Result = SWITCH_u8Pushed;
		}
	}

	else
	{
		u8Result =  SWITCH_u8Error;
	}

	return u8Result;
}


/*Description:this function should be called by OS
 * 				it takes void and returns void
 * 				the purpose is to handle sw bouncing
 * 				the period is 5 ms
 */
void switchHandler(void)
{
	static u8 prev[SWITCH_u8_SW_NB]={0};
	static u8 new[SWITCH_u8_SW_NB]={0};

	u8 i=0;

	for(i=0;i<SWITCH_u8_SW_NB;i++)
	{


		new[i]= DIO_u8GetPinValue(SWITCH_INSTANCES[i].SWITCH_u8SwitchToDioLink);

		if(new[i]==prev[i])
		{
			if(count[i]>=5)
			{
				swCurrentState[i]=new[i];
			}
			else
			{
				count[i]++;
			}
		}
		else
		{
			count[i]=1;
		}
		prev[i]=new[i];


	}

}
