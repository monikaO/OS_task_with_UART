################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../DIO_prog.c \
../GIE_prog.c \
../HandlingTasksUsingUART.c \
../IP_38.c \
../KEYPAD.c \
../KEYPAD_cfg.c \
../LEDS.c \
../LEDS_cfg.c \
../OS.c \
../OS_Config.c \
../SEGMENT.c \
../SEGMENT_cfg.c \
../SWITCH.c \
../SWITCH_cfg.c \
../Timer.c \
../Timer_Config.c \
../keypad_task.c \
../led1Task.c \
../led2Task.c \
../main.c \
../sevenSegmentsTask.c \
../sw1Task.c \
../sw2Task.c \
../switchModeTask.c \
../timeAdjustTask.c \
../timeCountTask.c \
../uart.c \
../uart_cfg.c 

OBJS += \
./DIO_prog.o \
./GIE_prog.o \
./HandlingTasksUsingUART.o \
./IP_38.o \
./KEYPAD.o \
./KEYPAD_cfg.o \
./LEDS.o \
./LEDS_cfg.o \
./OS.o \
./OS_Config.o \
./SEGMENT.o \
./SEGMENT_cfg.o \
./SWITCH.o \
./SWITCH_cfg.o \
./Timer.o \
./Timer_Config.o \
./keypad_task.o \
./led1Task.o \
./led2Task.o \
./main.o \
./sevenSegmentsTask.o \
./sw1Task.o \
./sw2Task.o \
./switchModeTask.o \
./timeAdjustTask.o \
./timeCountTask.o \
./uart.o \
./uart_cfg.o 

C_DEPS += \
./DIO_prog.d \
./GIE_prog.d \
./HandlingTasksUsingUART.d \
./IP_38.d \
./KEYPAD.d \
./KEYPAD_cfg.d \
./LEDS.d \
./LEDS_cfg.d \
./OS.d \
./OS_Config.d \
./SEGMENT.d \
./SEGMENT_cfg.d \
./SWITCH.d \
./SWITCH_cfg.d \
./Timer.d \
./Timer_Config.d \
./keypad_task.d \
./led1Task.d \
./led2Task.d \
./main.d \
./sevenSegmentsTask.d \
./sw1Task.d \
./sw2Task.d \
./switchModeTask.d \
./timeAdjustTask.d \
./timeCountTask.d \
./uart.d \
./uart_cfg.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -Wall -g2 -gstabs -O0 -fpack-struct -fshort-enums -ffunction-sections -fdata-sections -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=atmega32 -DF_CPU=8000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


