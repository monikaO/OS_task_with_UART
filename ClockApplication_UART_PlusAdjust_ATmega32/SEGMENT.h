/* A guard for the header file to insure it will be included only once                  */
#ifndef SEGMENT_H
#define SEGMENT_H
/****************************************************************************************/

#include "SEGMENT_cfg.h"

/* This function is used to enable the seven segment									*/
void SEGMENT_voidEnable(u8 u8SegmentIndex);

/* This function is used to disable the seven segment									*/
void SEGMENT_voidDisable(u8 u8SegmentIndex);

/* This function is used to output a specific number on the seven segment				*/
void SEGMENT_voidDisplayNumber(u8 u8NumberCpy, u8 u8SegmentIndex);
 
#endif
