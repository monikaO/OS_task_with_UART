#ifndef SEVENSEGMENTS_H
#define SEVENSEGMENTS_H

/* Description: This function implements the seven segments task */ 
extern void taskSegments(void);

#endif
