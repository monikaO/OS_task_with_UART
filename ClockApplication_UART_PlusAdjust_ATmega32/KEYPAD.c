#include "STD_TYPES.h"
#include "BIT_MATH.h"

#include "DIO_int.h"

#include "KEYPAD.h"

static u16 u16ResultLoc = 0;
static void voidSetSwResult(u8 u8ColCpy);
/******************************************/
/* Description: this function shall return */
/*              the status of the keypads  */
/*				      switches each switch has a */
/*				      dedicated bit */
/******************************************/
extern u16 KEYPAD_u16GetStatus(void)
{
	u16ResultLoc = 0;

	/* Phase 1 */
	DIO_voidSetPinValue(KEYPAD_u8_OUT_1, DIO_u8_LOW);
	DIO_voidSetPinValue(KEYPAD_u8_OUT_2, DIO_u8_HIGH);
	DIO_voidSetPinValue(KEYPAD_u8_OUT_3, DIO_u8_HIGH);
	DIO_voidSetPinValue(KEYPAD_u8_OUT_4, DIO_u8_HIGH);
	voidSetSwResult(0);

	/* Phase 2 */
	DIO_voidSetPinValue(KEYPAD_u8_OUT_1, DIO_u8_HIGH);
	DIO_voidSetPinValue(KEYPAD_u8_OUT_2, DIO_u8_LOW);
	DIO_voidSetPinValue(KEYPAD_u8_OUT_3, DIO_u8_HIGH);
	DIO_voidSetPinValue(KEYPAD_u8_OUT_4, DIO_u8_HIGH);
	voidSetSwResult(1);

	/* Phase 3 */
	DIO_voidSetPinValue(KEYPAD_u8_OUT_1, DIO_u8_HIGH);
	DIO_voidSetPinValue(KEYPAD_u8_OUT_2, DIO_u8_HIGH);
	DIO_voidSetPinValue(KEYPAD_u8_OUT_3, DIO_u8_LOW);
	DIO_voidSetPinValue(KEYPAD_u8_OUT_4, DIO_u8_HIGH);
	voidSetSwResult(2);

	/* Phase 4 */
	DIO_voidSetPinValue(KEYPAD_u8_OUT_1, DIO_u8_HIGH);
	DIO_voidSetPinValue(KEYPAD_u8_OUT_2, DIO_u8_HIGH);
	DIO_voidSetPinValue(KEYPAD_u8_OUT_3, DIO_u8_HIGH);
	DIO_voidSetPinValue(KEYPAD_u8_OUT_4, DIO_u8_LOW);
	voidSetSwResult(3);
	return u16ResultLoc;

}
static void voidSetSwResult(u8 u8ColCpy)
{

	if(DIO_u8GetPinValue(KEYPAD_u8_INP_1) == DIO_u8_LOW)
	{
		SET_BIT(u16ResultLoc, u8ColCpy);
	}
	if(DIO_u8GetPinValue(KEYPAD_u8_INP_2) == DIO_u8_LOW)
	{
		SET_BIT(u16ResultLoc, u8ColCpy+4);
	}
	if(DIO_u8GetPinValue(KEYPAD_u8_INP_3) == DIO_u8_LOW)
	{
		SET_BIT(u16ResultLoc, u8ColCpy+8);
	}
	if(DIO_u8GetPinValue(KEYPAD_u8_INP_4) == DIO_u8_LOW)
	{
		SET_BIT(u16ResultLoc, u8ColCpy+12);
	}

}
