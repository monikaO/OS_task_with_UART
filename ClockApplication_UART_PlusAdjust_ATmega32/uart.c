/*include the required headers*/
#include "STD_TYPES.h"
#include "BIT_MATH.h"
#include "uart.h"
#include "GIE_int.h"

/*define the needed registers needed by the uart*/
#define UBRRH *((volatile u8*)0x40)
#define UBRRL *((volatile u8*)0x29)
#define UCSRA *((volatile u8*)0x2B)
#define UCSRB *((volatile u8*)0x2A)
#define UCSRC *((volatile u8*)0x40)
#define UDR   *((volatile u8*)0x2C)

/*the length and the receive buffer should be updated in the ISR*/
volatile static u8 RX_ARRAY[RECIEVE_BUFFER_LENGTH];
volatile static u16 RX_LEN=0;

/****** bits of UCSRB ******/
#define UCSZ2 2
#define RXCIE 7
#define RXEN 4
#define TXEN 3

/***** bits of UCRSA ****/
#define UDRE 5

/*** bits of UCSRC ***/
#define URSEL	7
#define UMSEL	6
#define UPM1	5
#define UPM0	4
#define USBS	3
#define UCSZ1	2
#define UCSZ0	1
#define UCPOL	0

extern void UART_init(void)
{
	/* equation to calculate the value that will be loaded for a specific baud rate */
	u16 baud = (u16)((u16)((u32)SYSTEM_FREQUENCY/((u32)((u8)16*(u32)uart_cfg[0].baudRate))) - (u16)1);

	/* temporary variable to save the required configurations for UCSRC register */
	u8 UCSRC_temp = 0;

	/* save the value of baud rate in UBRR */
	UBRRH = (u8)(baud>>8);
	UBRRL = (u8)baud;

	/*ensure that all the register values are set to the default*/
	UCSRA=0x00;

	/* check on chosen parity and set the specific bits using the datasheet */
	if(uart_cfg[0].parity == NO_PARITY)
	{
		CLR_BIT(UCSRC_temp,UPM0);
		CLR_BIT(UCSRC_temp,UPM1);
	}else if(uart_cfg[0].parity == EVEN_PARITY)
	{
		SET_BIT(UCSRC_temp,UPM1);
		CLR_BIT(UCSRC_temp,UPM0);
	}else if(uart_cfg[0].parity == ODD_PARITY)
	{
		SET_BIT(UCSRC_temp,UPM0);
		SET_BIT(UCSRC_temp,UPM1);
	}else
	{
		/* do nothing */
	}

	/* check on chosen data length and set the specific bits using the datasheet */
	if(uart_cfg[0].dataLen == FIVE_BITS)
	{
		CLR_BIT(UCSRC_temp,UCSZ0);
		CLR_BIT(UCSRC_temp,UCSZ1);
	}else if(uart_cfg[0].dataLen == SIX_BITS)
	{
		SET_BIT(UCSRC_temp,UCSZ0);
		CLR_BIT(UCSRC_temp,UCSZ1);
	}else if(uart_cfg[0].dataLen == SEVEN_BITS)
	{
		CLR_BIT(UCSRC_temp,UCSZ0);
		SET_BIT(UCSRC_temp,UCSZ1);
	}else if(uart_cfg[0].dataLen == EIGHT_BITS)
	{
		SET_BIT(UCSRC_temp,UCSZ0);
		SET_BIT(UCSRC_temp,UCSZ1);
	}else if(uart_cfg[0].dataLen == NINE_BITS)
	{
		SET_BIT(UCSRC_temp,UCSZ0);
		SET_BIT(UCSRC_temp,UCSZ1);
		SET_BIT(UCSRB,UCSZ2);
	}else
	{
		/* do nothing */
	}

	/* check on chosen stop bits length and set the specific bits using the datasheet */
	if(uart_cfg[0].stopLen == ONE_STOP)
	{
		CLR_BIT(UCSRC_temp,USBS);
	}else if(uart_cfg[0].stopLen == TWO_STOPS)
	{
		SET_BIT(UCSRC_temp,USBS);
	}else
	{
		/* do nothing */
	}

	/* set bit URSEL to choose to write on UCSRC register */
	SET_BIT(UCSRC_temp,URSEL);

	/* asynchronous mode is used by default */
	CLR_BIT(UCSRC_temp,UMSEL);

	/* unused bit is cleared */
	CLR_BIT(UCSRC_temp,UCPOL);

	/*write on UCSRC at once*/
	UCSRC = UCSRC_temp;

	/* Enable receiver and transmitter */
	/*enable transmitter*/
	SET_BIT(UCSRB,RXEN);

	/*enable receiver*/
	SET_BIT(UCSRB,TXEN);

	/*enable RXCIE: RX Complete Interrupt Enable*/
	SET_BIT(UCSRB,RXCIE);

}


/*transmit function*/
extern void UART_transmit(u8* data,u16 len)
{
	u16 i;

	/*loop for the required length in order to sent all the length of the data*/
	for(i=0;i<len;i++)
	{
		//check the flag to ensure that the previous byte is sent*/
		//poll until the end
		 while (!( UCSRA & (1<<UDRE)));

		/* read data and save it in UDR to be transmitted */
		UDR=data[i];
	}
}

/*Receive function this function should return array of the received data*/
extern void UART_receive(u8* data,u16* len)
{
	u16 i;

	/*disable the interrupt at the beginning of receiving*/
	SET_BIT(UCSRB,RXCIE);

	/* loop on data and save it in the sent parameter */
	for(i=0;i<RX_LEN;i++)
	{
		data[i]= RX_ARRAY[i];
	}
	*len=RX_LEN;

	/*reset the length for the next interrupt to begin receiving from the beginning */
	RX_LEN=0;

	/*enable the interrupt at the end of receiving*/
	CLR_BIT(UCSRB,RXCIE);
}


/*define the uart ISR vector*/
/*USART, Rx Complete*/
void __vector_13 (void) __attribute__((signal,used));
void __vector_13 (void)
{
	RX_ARRAY[RX_LEN]=UDR;
	RX_LEN++;
}
