#ifndef _SW_MODE_TASK_H
#define _SW_MODE_TASK_H

#define u8_SEC_MODE 0
#define u8_MIN_MODE 1
#define u8_HR_MODE 2

/*this task check the keypad to switch the clock display mode on 7 segment*/
void SW_Clock_Mode_Task(void);

/*this function returns the current clock display mode*/
/*0 for sec*/
/*1 for min*/
/*2 for hr */
/*you can use the #defines in the switchMoseTask.h instead*/
u8 u8_get_Clock_mode();

#endif

