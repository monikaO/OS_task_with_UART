#ifndef GIE_INT_H
#define GIE_INT_H

/* Description: This function enables the general interrupt */
void GIE_voidSetGIE(void);

/* Description: This function disables the general interrupt */
void GIE_voidClrGIE(void);

#endif