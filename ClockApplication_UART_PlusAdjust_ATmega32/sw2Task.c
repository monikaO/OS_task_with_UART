#include "STD_TYPES.h"
#include "SWITCH.h"
#include "sw2Task.h"

volatile static u8 sw2Flag;

/* Description: This function is used to return switch state flag */
u8 u8_get_SW2_STATE_FLAG(void)
{
	return sw2Flag;
}

/* Description: This function is used to implement switch 2 task */
void void_SW2_Task(void)
{
	/* check state for switch using interface from driver */
	if( GetSwitchState(SWITCH_u8_SW2)==SWITCH_u8Pushed )
	{
		/* update flag with pressed state */
		sw2Flag=  SW2TASK_STATE_FLAG_PUSHED ;
	}
	else
	{
		/* update flag with released state */
		sw2Flag=SW2TASK_STATE_FLAG_RELEASED ;
	}
}

