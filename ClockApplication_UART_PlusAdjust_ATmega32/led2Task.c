#include "STD_TYPES.h"

#include "SWITCH.h"
#include "LEDS.h"

#include "sw2Task.h"

#include "HandlingTasksUsingUART.h"

/* Description: This function implements led 2 task */
void void_LED2_Task(void)
{

	u8 sw2_state = i38_getSW2State();
	if(sw2_state == SWITCH_u8Pushed)
	{
		LEDS_voidSetLedOn(1);
	}
	else
	{
		LEDS_voidSetLedOff(1);
	}
}
