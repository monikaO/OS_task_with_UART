#include "STD_TYPES.h"
#include "LEDS.h"
#include "sw1Task.h"

#include "SWITCH.h"
#include "HandlingTasksUsingUART.h"

/* Description: This function implements led 1 task */
void void_LED1_Task(void)
{
	u8 sw1_state = i38_getSW1State();
	if(sw1_state == SWITCH_u8Pushed)
	{
		LEDS_voidSetLedOn(0);
	}
	else
	{
		LEDS_voidSetLedOff(0);
	}
}
