#ifndef _KEYPAS_TASK_H_
#define  _KEYPAS_TASK_H_

#define Change_MODE_TO_SEC_OPTION 10
#define Change_MODE_TO_MIN_OPTION 11
#define Change_MODE_TO_HR_OPTION 12

#define ADJUST_SEC_OPTION 13
#define ADJUST_MIN_OPTION 14
#define ADJUST_HR_OPTION 15

#define KEYPAD_NOT_PRESSED 16
#define KEYPAD_ERROR 255


/*description: this function do the keypad task update the flags and should be called by os every 100ms*/
void void_keypad_task(void);

/*description: this function returns the first pressed key on the keypad*/
u8 u8_get_first_pressed_key(void);

/*this function returns 1 if it is the first time to press the switch or 0 otherwise*/
u8 u8_first_time_pressed(void);

/*this function returns 1 if it is the first timer to release the switch or 0 otherwise*/
u8 u8_first_time_released(void);

/*this function returns if it is the first timer to release the switch or not*/
u8 u8_first_time_Released(void);

/*this function returns the number of ticks from the first press to the function calling time*/
u16 u16_get_press_time(void);

/*this function returns the number of ticks from the first release to the function calling time*/
u16 u16_get_release_time(void);

#endif


