#include"STD_TYPES.h"
#include"BIT_MATH.h"
#include"GIE_reg.h"

/* Description: This function enables the general interrupt */
void GIE_voidSetGIE(void){
	SET_BIT(SREG,7);
}

/* Description: This function disables the general interrupt */
void GIE_voidClrGIE(void){
		CLR_BIT(SREG,7);
}