#include "STD_TYPES.h"
#include "LEDS_cfg.h"
#include "DIO_int.h"


const Led leds[] = {
         {LEDS_u8_MODE_NORMAL,DIO_u8_PIN_10,LEDS_u8_STATE_LOW},
         {LEDS_u8_MODE_NORMAL,DIO_u8_PIN_11,LEDS_u8_STATE_LOW},
};

const u8 led_max = sizeof(leds)/sizeof(Led);
