#ifndef SWITCH_CFG_H_
#define SWITCH_CFG_H_

typedef struct
{
	u8 SWITCH_u8SwitchOpenState;
	u8 SWITCH_u8SwitchToDioLink;
}SW_Struct;


typedef enum{
	SWITCH_u8_SW1,
	SWITCH_u8_SW2,
	/*
	 * Please insert your switches here
	 */
	SWITCH_u8_SW_NB
}SWITCH_SWITCHIndex;


extern const SW_Struct SWITCH_INSTANCES[SWITCH_u8_SW_NB];

#endif /* SWITCH_CFG_H_ */
