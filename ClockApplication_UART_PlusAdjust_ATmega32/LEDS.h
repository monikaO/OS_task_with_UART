#ifndef _LEDS_INT_H
#define _LEDS_INT_H

#include "LEDS_cfg.h"

/*Initializing LEDS*/
extern void LEDS_voidInitialize(void);

/* Turning chosen LED on*/
extern void LEDS_voidSetLedOn(u8 u8LedPinIndex);

/* Turning chosen LED off*/
extern void LEDS_voidSetLedOff(u8 u8LedPinIndex);

#endif
