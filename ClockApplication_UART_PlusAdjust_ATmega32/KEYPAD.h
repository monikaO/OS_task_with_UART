#ifndef KEYPAD_H
#define KEYPAD_H

#include "KEYPAD_cfg.h"

/* Description: this function shall return */
/*              the status of the keypads  */
/*				      switches each switch has a */
/*				      dedicated bit */
extern u16 KEYPAD_u16GetStatus(void);

#endif