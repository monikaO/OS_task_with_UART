#include "STD_TYPES.h"
#include "uart_cfg.h"

/*define array that represents the array that holds the real configuration of the driver*/
const uart_cfg_t uart_cfg[]=
{
		{(u32)250000,NO_PARITY,EIGHT_BITS,ONE_STOP}
};

