#include "STD_TYPES.h"

#include "keypad_task.h"

#include "timeCountTask.h"

#include "HandlingTasksUsingUART.h"

extern void taskTimeAdjust(void)
{
	/* Define static variable */
	static u8 timeCounter = 0;
	static u8 flagAdjust = 0;
	static u8 flagOnes = 0;
	static u8 mode = 0 ;
	static u8 onesTemp = 0;
	static u8 tensTemp = 0;
	static u8 flagOnesTemp = 0;

	u8 pressedKey, number;

	pressedKey = i38_getKeypadPressedKey();


	/* check is pressed key is one of the adjust keys */
	if(pressedKey == ADJUST_SEC_OPTION || pressedKey == ADJUST_MIN_OPTION || pressedKey == ADJUST_HR_OPTION)
	{
		/* if yes, update flagAdjust to 1 (true)*/
		flagAdjust = 1;

		/* switch on pressed key to update mode */
		switch(pressedKey)
		{
		case ADJUST_SEC_OPTION :
			/* seconds mode */
			mode = 1;
			break;
		case ADJUST_MIN_OPTION :
			/* minutes mode */
			mode = 2;
			break;
		default :
			/* hours mode */
			mode = 3;
			break;
		}
	}

	/* if key is not one of the adjust then user is entering the numbers */
	/* check if in the middle of adjusting */
	else if(flagAdjust)
	{
		/* check if keypad is not pressed */
		if(pressedKey==KEYPAD_NOT_PRESSED)
		{
			/* check if flagOnesTemp is true */
			if(flagOnesTemp)
			{
				/* set flagOnes to true */
				flagOnes = 1;

				/* reset timeCounter to count 2 secs for the second number */
				timeCounter = 0;
			}

			/* check if flagOnesTemp is false */
			else if(flagOnesTemp==0)
			{
				/* increment timeCounter as no pressed key yet */
				timeCounter++;
			}

			/* check if flagOnes is true */
			if(flagOnes)
			{
				/* increment timeCounter as second number is not yet entered */
				timeCounter++;
			}
		}
		/* check if a number is pressed */
		if(pressedKey >=0 && pressedKey <=9)
		{

			/* check if second number to enter and switch released */
			if(flagOnes)
			{
				/* update the tens number with the pressedKey */
				tensTemp = pressedKey;

				/* write the whole number to that is to be written on seven segments */
				number = (10*tensTemp) + onesTemp;

				/* switch on mode */
				switch(mode)
				{
				case 1:
					/* set number to seconds */
					setSeconds(number);
					break;
				case 2:
					/* set number to minutes */
					setMinutes(number);
					break;
				default:
					/* set number to hours */
					setHours(number);
				}

				/* reset behavior */
				flagAdjust = 0;
				flagOnesTemp = 0;
				flagOnes = 0;
				mode = 0;
				timeCounter = 0;

			}

			/* check if first number to enter */
			else 
			{
				/* update the ones number with the pressed key */
				onesTemp = pressedKey;

				/* update the ones flag */
				flagOnesTemp = 1;
			}
		}

		/* check if user stayed idle for more than 2 seconds */
		/* time to enter one of the two numbers exceeded two seconds */
		if(timeCounter>=20)
		{
			/* reset behavior */
			flagAdjust = 0;
			flagOnesTemp = 0;
			flagOnes = 0;
			mode = 0;
			timeCounter = 0;
		}
	}
}
