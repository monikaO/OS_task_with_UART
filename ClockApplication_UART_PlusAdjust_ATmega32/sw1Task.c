#include "STD_TYPES.h"
#include "SWITCH.h"
#include "sw1Task.h"

volatile static u8 sw1Flag;

/* Description: This function is used to return switch state flag */
u8 u8_get_SW1_STATE_FLAG(void)
{
	return sw1Flag;
}

/* Description: This function is used to implement switch 1 task */
void void_SW1_Task(void)
{
	/* check state for switch using interface from driver */
	if( GetSwitchState(SWITCH_u8_SW1)==SWITCH_u8Pushed )
	{

		/* update flag with pressed state */
		sw1Flag= SW1TASK_STATE_FLAG_PUSHED;

	}
	else
	{

		/* update flag with released state */
		sw1Flag=SW1TASK_STATE_FLAG_RELEASED;

	}
}

