#ifndef _KEYPAD_CONF_H
#define _KEYPAD_CONF_H

#define KEYPAD_u8_INP_1  DIO_u8_PIN_27
#define KEYPAD_u8_INP_2  DIO_u8_PIN_26
#define KEYPAD_u8_INP_3  DIO_u8_PIN_15
#define KEYPAD_u8_INP_4  DIO_u8_PIN_14

#define KEYPAD_u8_OUT_1  DIO_u8_PIN_31
#define KEYPAD_u8_OUT_2  DIO_u8_PIN_30
#define KEYPAD_u8_OUT_3  DIO_u8_PIN_29
#define KEYPAD_u8_OUT_4  DIO_u8_PIN_28

#endif

