#ifndef OS_CONFIG_H_
#define OS_CONFIG_H_

typedef struct
{
	void(*pftask)(void);
	u16	Periodicity;
}OS_Config_T;

extern const OS_Config_T OS_Config[];

extern const u8 Num_of_Tasks;

#endif /* OS_CONFIG_H_ */
