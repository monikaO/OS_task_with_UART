#ifndef UART_H
#define UART_H

/*include the configuration file*/
#include "uart_cfg.h"

/*define the required interfaces for different functions*/

/*initialize function that set the baud rate frame format(parit,stop,data bits)*/
extern void UART_init(void);

/*transmit function*/
extern void UART_transmit(u8* data,u16 len);

/*Receive function this function should return array of the received data*/
extern void UART_receive(u8* data,u16* len);

#endif

