/*interface file will be constant for different targets */
#ifndef Timer_H
#define Timer_H

#include "Timer_Config.h"

/******************************************/
/* Description: This function is used to  */
/* 				initialize the timer 	  */
/* 				peripheral				  */
/******************************************/
extern	void Timer_init(void);

/******************************************/
/* Description: This function is used to  */
/* 				Enable timer interrupt	  */
/******************************************/
extern	void Timer_EnableInterrupt(void);

/******************************************/
/* Description: This function is used to  */
/* 				Disable timer interrupt	  */
/******************************************/
extern	void Timer_DisableInterrupt(void);

/******************************************/
/* Description: This function is used to  */
/* 				set timer call back 	  */
/* 				function 	  			  */
/******************************************/
extern	void Timer_SetCallBack(void (*ptrCpy) (void));

#endif
